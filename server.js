const express = require("express");
const path = require("path");

const port = 3000;

const app = express();

app.set("view engine", "ejs");
app.set("views", path.join(__dirname, "./views"));

app.use(express.static(path.join(__dirname, "./static")));

app.get("/", (req, res) => {
  res.sendFile(path.join(__dirname, "./static/index.html"));
});

// app.get("/", function(request, response){
//   response.render("index.ejs", { titulo: "Olá mundo!" });
// });

app.listen(port, () => {
  console.log(`Servidor rodando na porta ${port}`);
});
